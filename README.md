### layui步骤条插件
基于[Layui][1]模块化的开源组件

#### 截图展示
![N11Kx0.png](https://s1.ax1x.com/2020/06/21/N11Kx0.png)

#### 快速使用
```javascript
//定义数据，这里是为了模拟展示
let data0 = {
    //步骤细节
    steps: [{"title" : "提交初审", "time" : "2018-07-22"},
            {"title" : "初审完成", "time" : "2018-07-22"},
            {"title" : "提交复审", "time" : "2018-07-22"},
            {"title" : "复审完成", "time" : "2018-07-22"},
            {"title" : "处理完成", "time" : "2018-07-22"}]
    //当前在第几步
    ,current: 0
}
layui.config({
    base: 'layui/mods/extend/step/'
}).use('step',function(){
    var step = layui.step;
    step.ready({
        elem: '#demo0',
        data: data0,
        width: '150px',
        color: {
            //支持HEX RGB
            success:'green',
            error:'red'
        }
    })
})
```
#### 项目推荐

[sliderVerify - 滑块验证][2]

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

[1]:https://www.layui.com
[2]:https://gitee.com/jishudaquan/sliderValidate